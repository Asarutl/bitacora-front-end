import Base from '@/modules/auth/base'
import Login from '@/modules/auth/components/login'

const routes = [
    {
        path: '/auth',
        component: Base,
        children: [
            {
                path: 'login',
                component: Login,
                name: 'auth.login'
            }
        ]
    }
]


export default routes;
