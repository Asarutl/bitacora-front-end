import Base from '@/modules/user/base'
import Index from '@/modules/user/components/index'
import Home from "@/views/Home";




let routes = [
        {
            path: '/user',
            component: Base,
            children: [
                {
                    path: 'index',
                    component: Index,
                    name: 'user.index'
                }
            ]
        }
    ]
export default routes;
