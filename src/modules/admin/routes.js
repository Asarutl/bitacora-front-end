import Base from '@/modules/admin/base'
import Index from '@/modules/admin/components/index'
import Logs from '@/modules/admin/components/connections'
import BaseLog from '@/modules/auth/base'

let routes = [
        {
            path: '/admin',
            component: Base,
            children: [
                {
                    path: 'index',
                    component: Index,
                    name: 'admin.index'
                },
                {
                    path: 'logs',
                    component: Logs,
                    name: 'admin.logs'
                }
            ]
        }
    ]

export default routes;
