import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
import VueMeta from 'vue-meta';
import VueSweetalert2 from 'vue-sweetalert2';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import '@fortawesome/fontawesome-free/css/all.css'
import '@fortawesome/fontawesome-free/js/all.js'

Vue.use(VueSweetalert2);
Vue.use(VueMeta);
Vue.use(BootstrapVue);
Vue.use(require('vue-moment'));
