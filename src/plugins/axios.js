import axios from 'axios'
const SERVER_URL = 'http://localhost:8090/api';
const instance = axios.create({
    baseURL: SERVER_URL,
    timeout: 30000
})
export default {
    async execute(method, resource, data, config){
        return instance({
            method: method,
            url: resource,
            data,
            config,
        })
    },
    POST(endPoint, object){
        return this.execute('POST', endPoint, object)
    },
    GET(endPoint){
        return this.execute('GET', endPoint, null, {
            transformResponse: [function(data){
                return data;
            }]
        });
    },
    PUT(endPoint, object){
        return this.execute('PUT', endPoint, object)
    },
    DELETE(endPoint){
        return this.execute('DELETE',endPoint)
    },
}
