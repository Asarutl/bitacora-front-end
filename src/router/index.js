import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import AuthRoutes from '@/modules/auth/routes'
import UserRoutes from '@/modules/user/routes'
import AdminRoutes from '@/modules/admin/routes'


import {defaultRouteByRole} from './navigationGuards'
import BaseLog from "@/modules/auth/base";
Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter: defaultRouteByRole
  },
  {
    path: '*',
    name: 'all',
    component: BaseLog
  },
    ...AuthRoutes,
    ...UserRoutes,
    ...AdminRoutes

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.afterEach((to) => {
  Vue.nextTick(() => {
    document.title = to.name || 'WERE HOUSE';
  });
});
export default router

